package ru.t1.dzelenin.tm.api.repository;

import ru.t1.dzelenin.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    void add(Project project);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    void clear();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    int getSize();

    boolean existsById(String id);
}
