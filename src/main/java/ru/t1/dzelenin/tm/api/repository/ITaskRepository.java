package ru.t1.dzelenin.tm.api.repository;

import ru.t1.dzelenin.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task tasks);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

    List<Task> findAllByProjectId(String projectId);
}
